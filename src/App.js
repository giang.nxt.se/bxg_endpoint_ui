import HomePage from "./components/HomePage";

function App() {
  return (
    <div
      style={{
        marginTop: "100px",
        display: "flex",
        justifyContent: "center",
        alignItems: "flex-start",
      }}
    >
      <HomePage />
    </div>
  );
}

export default App;
