import axios from "axios";

export const axiosClient = axios.create({
  baseURL: "http://20.239.59.240:9000/",
  headers: {
    "Content-Type": "application/json",
  },
});
