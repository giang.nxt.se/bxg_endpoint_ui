import {
  Button,
  Checkbox,
  Form,
  Input,
  Modal,
  notification,
  Select,
  Tooltip,
} from "antd";
import TextArea from "antd/es/input/TextArea";
import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { axiosClient } from "../api/axiosClient";
import { reload, setOpenAdd } from "../store/reducer";

export default function AddEndpointModal() {
  const [another, setAnother] = useState(false);
  const [form] = Form.useForm();
  const [confirmLoading, setConfirmLoading] = useState(false);
  const { openAdd: open } = useSelector((state) => state.app);
  const dispatch = useDispatch();

  const handleOK = () => {
    dispatch(setOpenAdd(false));
    dispatch(reload());
  };
  const handleCancel = () => {
    dispatch(setOpenAdd(false));
    dispatch(reload());
  };

  const onFinish = async (values) => {
    setConfirmLoading(true);
    await axiosClient
      .post("/auth/endpoint_permission", {
        ...values,
        permissions: values.permissions ? values.permissions.split(",") : null,
      })
      .then((data) => {
        notification.success({
          duration: 2,
          message: "Success",
        });

        form.resetFields();

        if (!another) {
          handleOK();
        }
      })
      .catch((err) => {
        console.log(err);
        notification.error({
          duration: 2,
          message: err.message,
        });
      })
      .finally(() => setConfirmLoading(false));
  };

  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };

  return (
    <Modal
      title="Title"
      open={open}
      onOk={handleOK}
      confirmLoading={confirmLoading}
      onCancel={handleCancel}
      footer={null}
    >
      <Form
        name="basic"
        initialValues={{ remember: true }}
        onFinish={onFinish}
        labelCol={{ span: 5 }}
        onFinishFailed={onFinishFailed}
        form={form}
      >
        <Form.Item
          label="Endpoint"
          name="endpoint"
          rules={[{ required: true, message: "Please input endpoint!" }]}
        >
          <Input />
        </Form.Item>

        <Form.Item
          label="Method"
          name="method"
          rules={[{ required: true, message: "Please input Method!" }]}
        >
          <Select style={{ textAlign: "left" }}>
            <Select.Option style={{ textAlign: "left" }} value="GET">
              GET
            </Select.Option>
            <Select.Option style={{ textAlign: "left" }} value="POST">
              POST
            </Select.Option>
            <Select.Option style={{ textAlign: "left" }} value="PUT">
              PUT
            </Select.Option>
            <Select.Option style={{ textAlign: "left" }} value="DELETE">
              DELETE
            </Select.Option>
          </Select>
        </Form.Item>

        <Tooltip title="Token not required ">
          <Form.Item
            name="anonymous"
            valuePropName="checked"
            wrapperCol={{
              offset: 5,
              span: 16,
            }}
          >
            <Checkbox>Anonymous</Checkbox>
          </Form.Item>
        </Tooltip>

        <Tooltip title="Example: ANY,ADMIN,USER">
          <Form.Item label="Permission" name="permissions">
            <TextArea rows={4} />
          </Form.Item>
        </Tooltip>

        <Form.Item
          wrapperCol={{
            offset: 6,
            span: 16,
          }}
        >
          <Checkbox
            checked={another}
            onChange={(e) => setAnother(e.target.checked)}
          >
            Submit Another
          </Checkbox>

          <Button
            type="default"
            style={{ marginRight: 8 }}
            onClick={handleCancel}
          >
            Cancel
          </Button>
          <Button type="primary" htmlType="submit">
            OK
          </Button>
        </Form.Item>
      </Form>
    </Modal>
  );
}
