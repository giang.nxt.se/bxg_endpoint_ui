import {
  Button,
  Checkbox,
  Form,
  Input,
  Modal,
  notification,
  Select,
  Tooltip,
} from "antd";
import TextArea from "antd/es/input/TextArea";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { axiosClient } from "../api/axiosClient";
import { reload, setOpenDelete } from "../store/reducer";

export default function DeleteEndpointModal() {
  const [form] = Form.useForm();
  const [confirmLoading, setConfirmLoading] = useState(false);
  const { openDelete, deleteItem: item } = useSelector((state) => state.app);
  const dispatch = useDispatch();

  const handleCancel = () => {
    dispatch(setOpenDelete(false));
  };

  const handleOK = () => {
    dispatch(setOpenDelete(false));
    dispatch(reload());
  };

  useEffect(() => {
    if (item?.id) {
      form.setFieldsValue({
        id: item.id,
        endpoint: item.endpoint,
        method: item.method,
        anonymous: Boolean(item.anonymous),
        permissions: item.permissionsRender,
      });
    }
    // eslint-disable-next-line
  }, [item?.id, form]);

  const onFinish = async (values) => {
    setConfirmLoading(true);
    await axiosClient
      .delete(`/auth/endpoint_permission/${values.id}`)
      .then((data) => {
        notification.success({
          duration: 2,
          message: "Success",
        });

        form.resetFields();
        handleOK();
      })
      .catch((err) => {
        console.log(err);
        notification.error({
          duration: 2,
          message: err.message,
        });
      })
      .finally(() => setConfirmLoading(false));
  };

  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };

  return (
    <Modal
      title="Title"
      open={openDelete}
      onOk={handleOK}
      confirmLoading={confirmLoading}
      onCancel={handleCancel}
      footer={null}
    >
      <Form
        name="basic"
        onFinish={onFinish}
        labelCol={{ span: 5 }}
        onFinishFailed={onFinishFailed}
        form={form}
      >
        <Form.Item label="Id" name="id" rules={[{ required: true }]}>
          <Input value={item.id} disabled />
        </Form.Item>

        <Form.Item
          label="Endpoint"
          name="endpoint"
          rules={[{ required: true, message: "Please input endpoint!" }]}
        >
          <Input disabled />
        </Form.Item>

        <Form.Item
          label="Method"
          name="method"
          rules={[{ required: true, message: "Please input Method!" }]}
        >
          <Select style={{ textAlign: "left" }} disabled>
            <Select.Option style={{ textAlign: "left" }} value="GET">
              GET
            </Select.Option>
            <Select.Option style={{ textAlign: "left" }} value="POST">
              POST
            </Select.Option>
            <Select.Option style={{ textAlign: "left" }} value="PUT">
              PUT
            </Select.Option>
            <Select.Option style={{ textAlign: "left" }} value="DELETE">
              DELETE
            </Select.Option>
          </Select>
        </Form.Item>
        <Tooltip title="Token not required ">
          <Form.Item
            name="anonymous"
            valuePropName="checked"
            wrapperCol={{
              offset: 5,
              span: 16,
            }}
          >
            <Checkbox disabled>Anonymous</Checkbox>
          </Form.Item>
        </Tooltip>

        <Tooltip title="Example: ANY,ADMIN,USER">
          <Form.Item label="Permission" name="permissions">
            <TextArea rows={4} disabled />
          </Form.Item>
        </Tooltip>

        <Form.Item
          wrapperCol={{
            offset: 8,
            span: 16,
          }}
        >
          <Button
            type="default"
            style={{ marginRight: 8 }}
            onClick={() => {
              form.resetFields();
              handleCancel();
            }}
          >
            Cancel
          </Button>
          <Button type="primary" htmlType="submit" danger>
            Delete
          </Button>
        </Form.Item>
      </Form>
    </Modal>
  );
}
