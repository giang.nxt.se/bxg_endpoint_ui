import { Button, Space, Table, Tag, Typography } from "antd";
import { CheckOutlined, CloseOutlined } from "@ant-design/icons";
import { useDispatch, useSelector } from "react-redux";
import {
  setEdit,
  setOpenDelete,
  updateDeleteItem,
  updateEditItem,
} from "../store/reducer";

const { Text } = Typography;
const tagColor = {
  POST: "#81c994",
  PUT: "#e4a449",
  GET: "#85aef8",
  DELETE: "#d64e47",
};

export default function EndpointList() {
  const dispatch = useDispatch();
  const { data } = useSelector((state) => state.app);

  const onEditClick = (item) => {
    dispatch(updateEditItem(item));
    dispatch(setEdit(true));
  };
  const onDeleteClick = (item) => {
    dispatch(updateDeleteItem(item));
    dispatch(setOpenDelete(true));
  };

  const columns = [
    {
      title: "Endpoint",
      dataIndex: "endpoint",
      key: "endpoint",
      render: (text) => <Text strong>{text}</Text>,
    },
    {
      title: "Method",
      dataIndex: "method",
      key: "method",
      render: (text) => <Tag color={tagColor[text]}>{text}</Tag>,
    },
    {
      title: "Anonymous",
      dataIndex: "anonymous",
      key: "anonymous",
      render: (text) =>
        text ? (
          text === "true" ? (
            <Tag color="#81c994">
              <CheckOutlined />
            </Tag>
          ) : (
            <Tag color="#d64e47">
              <CloseOutlined />
            </Tag>
          )
        ) : (
          "_"
        ),
    },
    {
      title: "Permissions",
      dataIndex: "permissionsRender",
      key: "permissionsRender",
      render: (text) =>
        text !== "_" ? (
          <div>
            {text.split(",").map((x) => (
              <Tag color="blue">{x}</Tag>
            ))}
          </div>
        ) : (
          text
        ),
    },

    {
      title: "Action",
      key: "action",
      render: (_, record) => (
        <Space size="middle">
          <Button onClick={() => onEditClick(record)}>Edit</Button>
          <Button onClick={() => onDeleteClick(record)} danger>
            Delete
          </Button>
        </Space>
      ),
    },
  ];
  return (
    <div>
      <Table
        columns={columns}
        dataSource={data}
        pagination={false}
        scroll={{ y: 500 }}
      />
    </div>
  );
}
