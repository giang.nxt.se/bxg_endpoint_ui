import { Button, Card, Input } from "antd";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { axiosClient } from "../api/axiosClient";
import EndpointList from "./EndpointList";
import queryString from "query-string";
import AddEndpointModal from "./AddEndpointModal";
import EditEndpointModal from "./EditEndpointModal";
import DeleteEndpointModal from "./DeleteEndpointModal";
import { setData, setOpenAdd } from "../store/reducer";

export default function HomePage() {
  const [search, setSearch] = useState({ endpoint: "" });
  const { reload } = useSelector((state) => state.app);
  const dispatch = useDispatch();

  const NewButton = () => {
    return <Button onClick={() => dispatch(setOpenAdd(true))}>Add</Button>;
  };

  useEffect(() => {
    axiosClient
      .get(`/auth/endpoint_permission/list?${queryString.stringify(search)}`)
      .then((data) => {
        const dataList = data.data.map((item) => ({
          key: item.id,
          ...item,
          anonymous: item.anonymous ? String(item.anonymous) : "false",
          permissionsRender:
            item.permissions && item.permissions.length > 0
              ? item.permissions.toString()
              : "_",
        }));
        dispatch(setData(dataList));
      });
    // eslint-disable-next-line
  }, [search, reload]);
  return (
    <Card
      title="Endpoint Permission"
      style={{ width: 1000 }}
      extra={<NewButton />}
    >
      <Input
        placeholder="Search"
        value={search.endpoint}
        onChange={(e) => setSearch({ endpoint: e.target.value })}
      />

      <EndpointList />
      <AddEndpointModal />
      <EditEndpointModal />
      <DeleteEndpointModal />
    </Card>
  );
}
