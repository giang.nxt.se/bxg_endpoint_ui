import { createSlice } from "@reduxjs/toolkit";

const appSlice = createSlice({
  name: "app",
  initialState: {
    data: [],
    reload: false,
    openAdd: false,
    openEdit: false,
    editItem: {},
    openDelete: false,
    deleteItem: {},
  },
  reducers: {
    updateEditItem(state, { payload }) {
      state.editItem = payload;
    },
    updateDeleteItem(state, { payload }) {
      state.deleteItem = payload;
    },
    setEdit(state, { payload }) {
      state.openEdit = payload;
    },
    setData(state, { payload }) {
      state.data = payload;
    },
    setOpenAdd(state, { payload }) {
      state.openAdd = payload;
    },
    setOpenDelete(state, { payload }) {
      state.openDelete = payload;
    },
    reload(state) {
      state.reload = !state.reload;
    },
  },
});
const { actions, reducer } = appSlice;
export const {
  reload,
  updateEditItem,
  updateDeleteItem,
  setEdit,
  setData,
  setOpenAdd,
  setOpenDelete,
} = actions;
export default reducer;
