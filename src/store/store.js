import { combineReducers, configureStore } from "@reduxjs/toolkit";
import reducer from "./reducer";

const store = configureStore({
  reducer: combineReducers({
    app: reducer,
  }),
});

export { store };
